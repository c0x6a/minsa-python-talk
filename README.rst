Python Talk
============

Python talk to new programmers that comes to MINSA developers team.

This is a brief introduction to Python programming language, using a
Jupyter notebook and RISE to have interactive slides with a nice python
REPL online.


Running locally
----------------

Create a virtual environment, install dependencies from
``requirements.txt``.

Run: ``jupyter notebook``, open your browser and open the
``PythonTalk.ipynb`` notebook.

It will automatically be opened in **presentation mode**, use
``<left>`` and ``<right>`` arrow keys to navigate through the slides.
